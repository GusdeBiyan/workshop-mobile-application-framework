void main(List<String> args) async {
  print("Persiapan dalam hitungan ke 3 silahkan bernyanyi");
  print(await Line());
  print(await Line1());
  print(await Line2());
  print(await Lirik1());
  print(await Lirik2());
  print(await Lirik3());
  print(await Lirik4());
  print(await Lirik5());
  print(await Lirik6());
  print(await Lirik7());
  print(await Lirik8());
}

Future<String> Line() async {
  String intro = "1";
  return await Future.delayed(Duration(seconds: 2), () => (intro));
}

Future<String> Line1() async {
  String intro = "2";
  return await Future.delayed(Duration(seconds: 2), () => (intro));
}

Future<String> Line2() async {
  String intro = "3";
  return await Future.delayed(Duration(seconds: 2), () => (intro));
}

Future<String> Lirik1() async {
  String lirik = "Tanpa welas kowe lunga biyen kae";
  return await Future.delayed(Duration(seconds: 1), () => (lirik));
}

Future<String> Lirik2() async {
  String lirik = "Ra ono mesakne aku sitik wae";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

Future<String> Lirik3() async {
  String lirik = "Ngaboti tresna anyarmu lalu kau tinggalkan aku";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

Future<String> Lirik4() async {
  String lirik = "Tersakiti sendiri di malam itu";
  return await Future.delayed(Duration(seconds: 7), () => (lirik));
}

Future<String> Lirik5() async {
  String lirik = "Kowe lunga pas aku sayang-sayange";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

Future<String> Lirik6() async {
  String lirik = "Tanpa pamit kowe ngadoh ngono wae";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

Future<String> Lirik7() async {
  String lirik = "Aku ra ngerti salahku dan kau campakkan diriku";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

Future<String> Lirik8() async {
  String lirik = "Bersanding dengan kekasih barumu";
  return await Future.delayed(Duration(seconds: 8), () => (lirik));
}

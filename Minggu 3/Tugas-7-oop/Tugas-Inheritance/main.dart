import 'armor-titan.dart';
import 'atack-titan.dart';
import 'beast-titan.dart';
import 'human.dart';

void main(List<String> args) {
  ArmorTitan ar = ArmorTitan();
  AttackTitan at = AttackTitan();
  BeastTitan bt = BeastTitan();
  Human h = Human();

  ar.PowerPoint = 7;
  at.PowerPoint = 8;
  bt.PowerPoint = 9;
  h.PowerPoint = 10;

  print("Power Point Armor Titan : ${ar.PowerPoint}");
  print("Power Point Attack Titan : ${at.PowerPoint}");
  print("Power Point Beast Titan : ${bt.PowerPoint}");
  print("Power Point Human : ${h.PowerPoint}");
}

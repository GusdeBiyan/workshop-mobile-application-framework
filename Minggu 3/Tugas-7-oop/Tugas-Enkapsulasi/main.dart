import 'lingkaran.dart';

void main(List<String> args) {
  Lingkaran lingkaran; // inisialisasi persegi panjang
  double luasLingkaran; // inisialisasi tipe data luas kotak

  lingkaran =
      new Lingkaran(); //menginisialisasi atau mengaliskan kotak sebagai persegi panjang/ pointer menunjuk object persegipanjang
  lingkaran.setPhi(
      3.14); // set nilai panjang(pembeda dengan method getter dan setter adalah adanya setPanjang)
  lingkaran.setR(
      7.0); //set nilai lebar (pembeda dengan method getter dan setter adalah adanya setLebar)

  luasLingkaran = lingkaran.hitungLuas(); // allias luaskotak
  print(luasLingkaran); // mencetak luas kotak
}

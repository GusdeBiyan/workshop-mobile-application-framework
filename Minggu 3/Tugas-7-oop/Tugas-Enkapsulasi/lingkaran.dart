class Lingkaran {
  late double _phi; //initialisasi tipe data panjang
  late double _r; //initialisasi tipe data lebar
  void setPhi(double value) {
    if (value < 0) {
      // validasi jika nilai di bawah 0
      value *= -1; // akan di kalikan -1, misal -1 * -2 hasilnya 2
    }
    _phi = value; //alias
  }

  double getPhi() {
    //get panjang
    return _phi; // mengembalikan nilai get panjang
  }

  void setR(double value) {
    if (value < 0) {
      // validasi jika nilai di bawah 0
      value *= -1; // akan di kalikan -1, misal -1 * -2 hasilnya 2
    }
    _r = value; //alias
  }

  double getR() {
    //get panjang
    return _r; // mengembalikan nilai get panjang
  }

  double hitungLuas() {
    return this._phi * _r * _r; //mereturn hasil
  }
}
